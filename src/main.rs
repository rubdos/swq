use actix_web::{
    dev::ServiceRequest,
    error, get,
    http::header::{ContentDisposition, DispositionParam, DispositionType},
    middleware, put,
    web::{self, Data},
    App, Error, HttpResponse, HttpServer, ResponseError,
};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use actix_web_httpauth::middleware::HttpAuthentication;
use futures::StreamExt;
use tokio::sync::RwLock;

#[derive(thiserror::Error, Debug)]
pub enum QueueError {
    #[error("Authentication failed")]
    AuthenticationFailed,
    #[error("Nothing in the queue")]
    QueueEmpty,
}

/// Actix Web uses `ResponseError` for conversion of errors to a response
impl ResponseError for QueueError {
    fn error_response(&self) -> HttpResponse {
        match self {
            QueueError::AuthenticationFailed => HttpResponse::Forbidden().finish(),
            QueueError::QueueEmpty => HttpResponse::NoContent().finish(),
        }
    }
}

#[derive(Default)]
pub struct JobQueue {
    token: Option<String>,
    inner: std::collections::VecDeque<(String, web::Bytes)>,
    total_jobs: usize,
}

const MAX_SIZE: usize = 256 * 1024; // max payload size is 256k

#[put("/job/{name}")]
async fn put_job(
    params: web::Path<String>,
    data: Data<RwLock<JobQueue>>,
    mut payload: web::Payload,
) -> Result<HttpResponse, Error> {
    let queue = data.into_inner();

    let name = params.into_inner();

    // payload is a stream of Bytes objects
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk?;
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > MAX_SIZE {
            return Err(error::ErrorBadRequest("overflow"));
        }
        body.extend_from_slice(&chunk);
    }

    queue
        .write()
        .await
        .inner
        .push_back((name.clone(), body.into()));
    queue.write().await.total_jobs += 1;

    Ok(HttpResponse::Ok().body(format!("{name} added to queue")))
}

#[get("/metrics")]
async fn metrics(data: Data<RwLock<JobQueue>>) -> Result<HttpResponse, QueueError> {
    let queue = data.into_inner();
    let queue = queue.read().await;

    let mut body = String::new();

    body.push_str("# HELP swq_queue_length Total number of pending items in the queue\n");
    body.push_str("# TYPE swq_queue_length gauge\n");
    body.push_str(&format!("swq_queue_length {}\n", queue.inner.len()));
    body.push_str("# HELP swq_total_jobs Total number of jobs ever queued\n");
    body.push_str("# TYPE swq_total_jobs counter\n");
    body.push_str(&format!("swq_total_jobs {}\n", queue.total_jobs));

    Ok(HttpResponse::Ok().body(body))
}

#[get("/next")]
async fn next(data: Data<RwLock<JobQueue>>) -> Result<HttpResponse, QueueError> {
    let queue = data.into_inner();

    let mut queue = queue.write().await;
    if let Some((name, item)) = queue.inner.pop_front() {
        let cd = ContentDisposition {
            disposition: DispositionType::Attachment,
            parameters: vec![DispositionParam::Filename(name)],
        };
        Ok(HttpResponse::Ok().insert_header(cd).body(item))
    } else {
        Err(QueueError::QueueEmpty)
    }
}

async fn bearer_auth_validator(
    req: ServiceRequest,
    credentials: BearerAuth,
) -> Result<ServiceRequest, Error> {
    let queue = req
        .app_data::<Data<RwLock<JobQueue>>>()
        .ok_or(QueueError::AuthenticationFailed)?;
    let queue = queue.clone().into_inner();

    let queue = queue.read().await;
    if let Some(token) = &queue.token {
        if credentials.token() == token {
            Ok(req)
        } else {
            Err(QueueError::AuthenticationFailed.into())
        }
    } else {
        Ok(req)
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    let token = match std::env::var("SWQ_TOKEN") {
        Ok(token) => Some(token),
        Err(std::env::VarError::NotPresent) => None,
        Err(e) => {
            eprintln!("Could not parse SWQ_TOKEN: {}", e);
            return Err(std::io::Error::new(std::io::ErrorKind::InvalidInput, e));
        }
    };
    let state = JobQueue {
        token,
        ..Default::default()
    };
    let state = RwLock::new(state);
    let state = Data::new(state);

    HttpServer::new(move || {
        let auth = HttpAuthentication::bearer(bearer_auth_validator);
        App::new()
            .app_data(state.clone())
            .wrap(middleware::Logger::default())
            .service(metrics)
            .service(web::scope("").wrap(auth).service(next).service(put_job))
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}
