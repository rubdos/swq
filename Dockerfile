FROM rust as builder

COPY . .
RUN cargo build --release --all-features

RUN ls target/
RUN ls target/release

FROM ubuntu:20.04

COPY --from=builder target/release/swq /usr/local/bin/

CMD ["/usr/local/bin/swq"]
